#include "../DLL/Triangulo.h"
#include "../DLL/Rectangulo.h"
#include "../DLL/Material.h"
#include "../DLL/BaseJuego.h"
#include "../DLL/Circulo.h"
#include "../DLL//Sprite.h"
#include "../DLL/CollisionManager.h"
#include "../DLL/Camera.h"
#include "../DLL/Mesh.h"
#include "../DLL/MeshImporter.h"
#include "../DLL/InputManager.h"
#include "../DLL/Node.h"
#include "../DLL/Component.h"
#include "../DLL/CameraComponent.h"
#include "../DLL/MeshComponent.h"
#include <iostream>

class Game : public BaseJuego
{
public:
	Game();
	~Game();
protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	void OnDraw() override;
private:
	float i;
	float j;
	int timer;
	Camera * camera;
	Node* sceneNode;
	CollisionManager * manager;
	Material* mat1;
	Material* mat2;
	Mesh* m;
	Mesh* m2;
	MeshImporter* importer;
	InputManager* input;
	void IdaYVuelta(int  &t);
};

