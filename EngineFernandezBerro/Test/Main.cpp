#include "Game.h"

int main() 
{

	BaseJuego * game = new Game();

	//TODO: startup
	if (game->Inicio()) 
	{
		game->Loop();
	}

	game->Stop();

	delete game;
	std::cin.get();
	return 0;
}