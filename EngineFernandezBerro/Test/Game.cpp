#include "Game.h"



Game::Game() : 
	i(0),
	timer (0),
	j(0)
{
}


Game::~Game()
{
}

bool Game::OnStart() 
{
	manager = new CollisionManager();
	importer = new MeshImporter();
	sceneNode = new Node(render);
	//mat1 = new Material();
	//mat2 = new Material();
	input = new InputManager(ventana);
	//unsigned int programID1 = mat1->CargaShaders("TextureVertexShader.txt", "TextureFragmentShader.txt");
	//unsigned int programID2 = mat2->CargaShaders("TextureVertexShader.txt", "TextureFragmentShader.txt");

	sceneNode->AddChild(importer->LoadMesh("Rifle.fbx", "rifle_texture.bmp", render));
	sceneNode->GetChildAtIndex(1)->transform->SetPosicion(0.0f, 0.0f, 100.0f);
	sceneNode->GetChildAtIndex(1)->GetChildAtIndex(2)->transform->SetPosicion(0.0f, 0.0f, -20.0f);
	/*m2 = new  Mesh(render, "M4A1\\M4A1.FBX", "M4A1\\M4A1Tex.bmp", importer);
	m2->SetMaterial(mat2);
	m = new Mesh(render, "Piedras\\rock_c_01.obj", "Piedras\\CementSmall.bmp", importer);
	m->SetMaterial(mat1);*/
	

	camera = new Camera(render);
	camera->SetPosicion(0, 0, -5);
	camera->Draw();
	cout << "Inicio Game" << endl;
	return true;
}

bool Game::OnStop() 
{
	cout << "Stop Game" << endl;
	/*delete m;
	delete m2;
	delete manager;
	delete mat1;
	delete mat2;*/
	delete importer;
	return false;
}

bool Game::OnUpdate() 
{
	timer += 0.1f;
	IdaYVuelta(timer);
	sceneNode->GetChildAtIndex(1)->transform->SetRotacion(0, i, 0);
	sceneNode->GetChildAtIndex(1)->GetChildAtIndex(2)->transform->SetRotacion(0, -i, 0);

	float cameraSpeed = 0.1f;

	if (input->isInput(GLFW_KEY_Q))//YAW LEFT
		camera->Yaw(cameraSpeed);
	if (input->isInput(GLFW_KEY_E))//YAW RIGHT
		camera->Yaw(-cameraSpeed);

	if (input->isInput(GLFW_KEY_A))//STRAFE LEFT
		camera->Strafe(cameraSpeed);
	if (input->isInput(GLFW_KEY_D))//STRAFE RIGHT
		camera->Strafe(-cameraSpeed);

	if (input->isInput(GLFW_KEY_W))//FORWARD
		camera->Walk(cameraSpeed);
	if (input->isInput(GLFW_KEY_S))//BACK
		camera->Walk(-cameraSpeed);

	if (input->isInput(GLFW_KEY_R))//PITCH UP
		camera->Pitch(cameraSpeed);
	if (input->isInput(GLFW_KEY_F))//PITCH DOWN
		camera->Pitch(-cameraSpeed);

	camera->Draw();
	
	manager->CheckCollisions();

	cout << "Update Game" << i << endl;
	return true;
}

void Game::OnDraw()
{
	sceneNode->Draw();
	cout << "Dibujado Game" << i << endl;
}

void Game::IdaYVuelta(int  &t)
{
	if (t <= 50)
		i += 0.01f;
	else if (t > 50 && t < 101)
		i -= 0.01f;
	else if (t == 101)
		t = 0;
}