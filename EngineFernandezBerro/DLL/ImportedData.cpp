#include "ImportedData.h"



ImportedData::ImportedData():
	vertexBuffer(NULL),
	indexBuffer(NULL),
	uvBuffer(NULL),
	numIndices(0),
	materialIndex(0)
{
	
}


ImportedData::~ImportedData()
{
	if (vertexBuffer != NULL) glDeleteBuffers(1, &vertexBuffer);
	if (indexBuffer != NULL) glDeleteBuffers(1, &indexBuffer);
}

void ImportedData::Init(const std::vector<Vertex>& Vertices, const std::vector<unsigned short>& Indices, Renderer* renderer)
{
	numIndices = Indices.size();

	float* positions = new float[Vertices.size() * 3];
	float* textures = new float[Vertices.size() * 2];
	float* normals = new float[Vertices.size() * 3];

	for (size_t i = 0; i < Vertices.size(); i++)
	{
		positions[i * 3] = Vertices[i].m_pos.x;
		positions[i * 3 + 1] = Vertices[i].m_pos.y;
		positions[i * 3 + 2] = Vertices[i].m_pos.z;
		textures[i * 2] = Vertices[i].m_tex.x;
		textures[i * 2 + 1] = Vertices[i].m_tex.y;
		normals[i * 3] = Vertices[i].m_normal.x;
		normals[i * 3 + 1] = Vertices[i].m_normal.y;
		normals[i * 3 + 2] = Vertices[i].m_normal.z;
	}

	vertexBuffer = renderer->GenerarBuffer(positions, sizeof(float) * Vertices.size() * 3);
	indexBuffer = renderer->GenerarMeshBuffer(Indices, Indices.size());
	uvBuffer = renderer->GenerarBuffer(textures, sizeof(float) * Vertices.size() * 2);
}