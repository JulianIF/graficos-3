#pragma once
#include "FormaColoreada.h"
#include "Export.h"
#include <vector>
#include "MeshImporter.h"
#include "ImportedData.h"

class DLL_API Mesh :
	public FormaColoreada
{
public:
	Mesh(Renderer * renderer, const string modelPath, string texturePath, MeshImporter* imp);
	~Mesh();
	void Draw() override;
	void SetIndices(std::vector<unsigned short> _vertices, int _count);
	bool LoadMesh(const string modelPath, string texturePath);
private:
	unsigned int IDMeshBuffer;
	bool meshBuffer;
	std::vector<unsigned short> indices;
	int countIndices;
	vector<unsigned int> bufferTextures;
	MeshImporter* importer;
	vector<ImportedData> m_Entries;
	vector<LoaderTexturas::Header> m_Textures;
};

