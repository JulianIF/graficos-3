#pragma once
#include "Export.h"
#include <iostream>
#include<glm\glm.hpp>
using namespace std;

class DLL_API BoxCollider
{
private:
	glm::vec2 position;
public:
	bool isTrigger;
	bool isStatic;
	float mass;
	int height;
	int width;
	BoxCollider();
	~BoxCollider();
	void SetPos(float _posX, float _posY);
	void SetSize(int _h, int _w);
};
