#pragma once
#include "FormaColoreada.h"
#include "Material.h"

class DLL_API Rectangulo :public FormaColoreada
{
public:
	Rectangulo(Renderer* render);
	~Rectangulo();
	void Draw() override;
};

