#pragma once
#include "Export.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

class DLL_API Material
{
	unsigned int programID;
	unsigned int textureID;
public:
	Material();
	~Material();
	void Bind();
	void BindTexture(const char * name, unsigned int textureBufferID);
	unsigned int CargaShaders(const char * vertex_file_path, const char * fragment_file_path);
	void SetMatrizProperty(const char* nombre, glm::mat4& mat);
};

