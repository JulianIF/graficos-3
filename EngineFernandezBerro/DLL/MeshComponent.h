#pragma once
#include "Export.h"
#include "Component.h"
#include "Material.h"
#include "LoaderTexturas.h"
#include "Node.h"

class DLL_API MeshComponent :
	public Component
{
private:
	Node* myNode;
	string modelPath;
	string texturePath;
	LoaderTexturas loader;
	LoaderTexturas::Header texture;
	Material* material;
	Renderer* rend;
	unsigned int programID;
	std::vector<float> vertices;
	std::vector<float> uvs;
	std::vector<unsigned short> facesIndex;

	unsigned int vertexBufferID;
	unsigned int uvBufferID;
	unsigned int IndexBufferID;
	unsigned int textureBufferID;
public:
	MeshComponent(Renderer* rend, Node* myNode);
	~MeshComponent();
	void Update();
	void Draw(Renderer* rend) override;

	void SetVertices(std::vector<float> vertices);
	void SetUVS(std::vector<float> uvs);
	void SetFaces(std::vector<unsigned short> faces);
	void SetTexture(string texturePath);

	void LoadMaterial();
};
