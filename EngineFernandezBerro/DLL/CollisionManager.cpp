#include "CollisionManager.h"



CollisionManager::CollisionManager()
{
}


CollisionManager::~CollisionManager()
{
}

void CollisionManager::AddToLayer(char _layer, Entidad * _e)
{
	cout << "Added to Layer" + _layer << endl;
	switch (_layer)
	{
	case 'A':
		layerA.push_back(_e);
		break;
	case 'B':
		layerB.push_back(_e);
		break;
	default:
		break;
	}
}

void CollisionManager::CheckCollisions()
{
	cout << "Checking Collisions" << endl;
	for (int i = 0; i < layerA.size(); i++)
	{
		for (int j = 0; j < layerB.size(); j++)
		{
			if (!layerA[i]->collider.isTrigger || !layerB[j]->collider.isTrigger)
			{
				float difX = layerA[i]->GetPosicion()[0] - layerB[j]->GetPosicion()[0];
				float difY = layerA[i]->GetPosicion()[1] - layerB[j]->GetPosicion()[1];

				bool colRight = false;
				bool colUp = false;

				float distMaxX = layerA[i]->collider.width / 2 + layerB[j]->collider.width / 2;
				float distMaxY = layerA[i]->collider.height / 2 + layerB[j]->collider.height / 2;

				float penX = distMaxX - abs(difX);
				float penY = distMaxY - abs(difY);

				if (difX < 0)
				{
					colRight = true;
				}
				if (difY < 0)
				{
					colUp = true;
				}

				if (abs(difX) <= distMaxX && abs(difY) <= distMaxY)
				{
					if (penX < penY)//PENETRACION HORIZONTAL
					{
						if (colRight)
						{
							if (layerA[i]->collider.isStatic)
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0] + penX, layerB[j]->GetPosicion()[1], layerB[j]->GetPosicion()[2]);
							else if (layerB[j]->collider.isStatic)
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0] - penX, layerA[i]->GetPosicion()[1], layerA[i]->GetPosicion()[2]);
							else
							{
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0] - (penX / 2), layerA[i]->GetPosicion()[1], layerA[i]->GetPosicion()[2]);
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0] + (penX / 2), layerB[j]->GetPosicion()[1], layerB[j]->GetPosicion()[2]);
							}
						}
						else 
						{
							if (layerA[i]->collider.isStatic)
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0] - penX, layerB[j]->GetPosicion()[1], layerB[j]->GetPosicion()[2]);
							else if (layerB[j]->collider.isStatic)
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0] + penX, layerA[i]->GetPosicion()[1], layerA[i]->GetPosicion()[2]);
							else
							{
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0] + (penX / 2), layerA[i]->GetPosicion()[1], layerA[i]->GetPosicion()[2]);
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0] - (penX / 2), layerB[j]->GetPosicion()[1], layerB[j]->GetPosicion()[2]);
							}
						}
					}
					else //PENETRACION VERTICAL
					{
						if (colUp)
						{
							if (layerA[i]->collider.isStatic)
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0], layerB[j]->GetPosicion()[1] + penY, layerB[j]->GetPosicion()[2]);
							else if (layerB[j]->collider.isStatic)
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0], layerA[i]->GetPosicion()[1] - penY, layerA[i]->GetPosicion()[2]);
							else
							{
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0], layerA[i]->GetPosicion()[1] - (penY / 2), layerA[i]->GetPosicion()[2]);
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0], layerB[j]->GetPosicion()[1] + (penY / 2), layerB[j]->GetPosicion()[2]);
							}
						}
						else
						{
							if (layerA[i]->collider.isStatic)
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0], layerB[j]->GetPosicion()[1] - penY, layerB[j]->GetPosicion()[2]);
							else if (layerB[j]->collider.isStatic)
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0], layerA[i]->GetPosicion()[1] + penY, layerA[i]->GetPosicion()[2]);
							else
							{
								layerA[i]->SetPosicion(layerA[i]->GetPosicion()[0], layerA[i]->GetPosicion()[1] + (penY / 2), layerA[i]->GetPosicion()[2]);
								layerB[j]->SetPosicion(layerB[j]->GetPosicion()[0], layerB[j]->GetPosicion()[1] - (penY / 2), layerB[j]->GetPosicion()[2]);
							}
						}
					}
				}
			}
		}
	}
}
