#include "MeshImporter.h"

#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"

MeshImporter::MeshImporter()
{

}


MeshImporter::~MeshImporter()
{
}

Node* MeshImporter::LoadMesh(const std::string& pFile, const std::string& texturePath, Renderer* renderer)
{
	bool Ret = false;
	Assimp::Importer Importer;

	const aiScene* pScene = Importer.ReadFile(pFile.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices);
	if (pScene) 
	{
		//Ret = InitFromScene(pScene, pFile, texturePath, m_Entries, m_Textures, renderer);
		Node * base = new Node(renderer);
		NodeHierarchy(pScene, base, pScene->mRootNode, texturePath, renderer);
		return base;
	}
	else {
		printf("Error parsing '%s': '%s'\n", pFile.c_str(), Importer.GetErrorString());
	}

	return false;
}

void MeshImporter::NodeHierarchy(const aiScene* scene, Node* baseNode, aiNode* root, const string& texturePath, Renderer* rend)
{
	for (int i = 0; i < root->mNumChildren; i++)
	{
		Node* childNode = new Node(rend);
		if (root->mChildren[i]->mNumMeshes > 0)
		{
			MeshComponent* meshComponent = new MeshComponent(rend, childNode);
			unsigned int index = root->mChildren[i]->mMeshes[0];
			InitMesh(scene->mMeshes[index], meshComponent, rend);
			meshComponent->SetTexture(texturePath);
			meshComponent->LoadMaterial();
			if (root->mChildren[i]->mNumChildren > 0)
			{
				NodeHierarchy(scene, childNode, root->mChildren[i], texturePath, rend);
			}
		}
		baseNode->AddChild(childNode);
	}
}

void MeshImporter::InitMesh(aiMesh* mesh, MeshComponent* meshComponent, Renderer* rend)
{
	std::vector<float> vertices;
	std::vector<float> uvs;
	std::vector<unsigned short> facesIndexes;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		aiVector3D pos = mesh->mVertices[i];
		vertices.push_back(pos.x);
		vertices.push_back(pos.y);
		vertices.push_back(pos.z);

		aiVector3D uv = mesh->mTextureCoords[0][i];
		uvs.push_back(uv.x);
		uvs.push_back(uv.y);
	}

	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		facesIndexes.push_back(mesh->mFaces[i].mIndices[0]);
		facesIndexes.push_back(mesh->mFaces[i].mIndices[1]);
		facesIndexes.push_back(mesh->mFaces[i].mIndices[2]);
	}

	meshComponent->SetVertices(vertices);
	meshComponent->SetUVS(uvs);
	meshComponent->SetFaces(facesIndexes);

}

int MeshImporter::CheckFormat(FILE*& file, unsigned char header[])
{
	if (!file) 
		return -1;

	if (header[0] == 'B' || header[1] == 'M') 
	{
		return 0; //Correct BMP
	}

	return -1;
}
