#pragma once
#include "Export.h"
#include <iostream>
#include<glm\glm.hpp>
#include "Component.h"

class DLL_API BoxColliderComponent :
	public Component
{
public:
	BoxColliderComponent(Renderer* rend);
	~BoxColliderComponent();

	bool isTrigger;
	bool isStatic;
	float mass;
	int height;
	int width;

	void SetPos(float _posX, float _posY);
	void SetSize(int _h, int _w);
private:
	glm::vec2 position;
};

