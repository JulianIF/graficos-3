#include "CameraComponent.h"



CameraComponent::CameraComponent(Renderer * renderer):Component(renderer)
{
	rend = renderer;
	transform = new Transform(renderer);
	up = glm::vec3(0, 1, 0);
	forward = glm::vec3(0, 0, 1);
	right = glm::vec3(1, 0, 0);
}


CameraComponent::~CameraComponent()
{
}

void CameraComponent::Draw(Renderer* rend)
{
	Update();
}

void CameraComponent::Update()
{
	up = glm::vec3(0, 1, 0);
	rend->SetViewMatrix(this->transform->GetPosicion(), this->transform->GetPosicion() + forward, up);
}

void CameraComponent::Start()
{

}

void CameraComponent::Walk(float dir)
{
	this->transform->SetPosicion(this->transform->GetPosicion().x + dir * forward.x, this->transform->GetPosicion().y + dir * forward.y, this->transform->GetPosicion().z + dir * forward.z);
	Update();
}

void CameraComponent::Strafe(float dir)
{
	this->transform->SetPosicion(this->transform->GetPosicion().x + dir * right.x, this->transform->GetPosicion().y + dir * right.y, this->transform->GetPosicion().z + dir * right.z);
	Update();
}

void CameraComponent::Pitch(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::vec4 aux(forward.x, forward.y, forward.z, 0);
	aux = rot * aux;
	forward = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(up.x, up.y, up.z, 0);
	aux = rot * aux;
	up = glm::vec3(aux.x, aux.y, aux.z);

	Update();
}

void CameraComponent::Roll(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(0.0f, 0.0f, 1.0f));
	glm::vec4 aux(right.x, right.y, right.z, 0);
	aux = rot * aux;
	right = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(up.x, up.y, up.z, 0);
	aux = rot * aux;
	up = glm::vec3(aux.x, aux.y, aux.z);

	Update();
}

void CameraComponent::Yaw(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::vec4 aux(forward.x, forward.y, forward.z, 0);

	aux = rot * aux;
	forward = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(right.x, right.y, right.z, 0);
	aux = rot * aux;
	right = glm::vec3(aux.x, aux.y, aux.z);

	Update();
}


