#pragma once
#include "Export.h"
#include "Renderer.h"
#include "Ventana.h"
#include "TypeDef.h"
#include "Node.h"
#include <iostream>

using namespace std;

class DLL_API BaseJuego
{
public:
	BaseJuego();
	~BaseJuego();
	bool Inicio();
	bool Stop();
	void Loop();
	
protected:
	Ventana* ventana;
	Renderer * render;
	virtual bool OnStart() = 0;
	virtual bool OnStop() = 0;
	virtual bool OnUpdate() = 0;
	virtual void OnDraw() = 0;
};

