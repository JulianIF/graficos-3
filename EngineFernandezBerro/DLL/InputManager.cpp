#include "InputManager.h"

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}


InputManager::InputManager(Ventana* _window)
{
	window = _window;
}


InputManager::~InputManager()
{
}

bool InputManager::isInput(int key)
{
	int state = glfwGetKey((GLFWwindow*)window->GetVentana(), key);
	if (state == GLFW_PRESS) return true;
	return false;
}

void InputManager::SetWindowContext(Ventana* window)
{
	this->window = window;
	glfwSetKeyCallback((GLFWwindow*)this->window->GetVentana(), KeyCallback);
}