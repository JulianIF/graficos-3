#include "Node.h"



Node::Node(Renderer * renderer):
	rend(renderer)
{
	transform = new Transform(renderer);
	AddComponent(transform);
	matTransform = glm::mat4(1.0f);
	Start();
}


Node::~Node()
{
}


void Node::AddChild(Node * n)
{
	children.push_back(*n);
}

void Node::AddComponent(Component * comp)
{
	components.push_back(comp);
}

void Node::Update()
{

	for (int i = 0; i < components.size(); i++)
	{
		components[i]->Update();
	}

	for (int i = 0; i < children.size(); i++)
	{
		children[i].Update();
	}
}

void Node::Start()
{

}

void Node::Draw()
{
	glm::mat4 aux = rend->GetMatrizMundo();
	rend->MultiplicarMatrizDeMundo(transform->GetWorldMatrix());

	for (int i = 0; i < components.size(); i++)
	{
		components[i]->Draw(rend);
	}

	for (int i = 0; i < children.size(); i++)
	{
		children[i].Draw();
	}
	rend->SetMatrizDeMundo(aux);
}

Node* Node::GetChildAtIndex(unsigned int i)
{
	return &children[i-1];
}
