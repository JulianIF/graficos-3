#include "MeshComponent.h"



MeshComponent::MeshComponent(Renderer* rend, Node* myNode):Component(rend)
{
	myNode->AddComponent(this);
}

MeshComponent::~MeshComponent()
{
}

void MeshComponent::Update()
{
}

void MeshComponent::Draw(Renderer* rend)
{
	if (material != NULL)
	{
		material->Bind();
		material->SetMatrizProperty("WVP", rend->GetMVP());
		material->BindTexture("myTextureSampler", textureBufferID);
	}
	rend->EmpezarDibujado(0);
	rend->EmpezarDibujado(1);
	rend->BindBuffer(vertexBufferID, 0);
	rend->BindUVBuffer(uvBufferID, 1);
	rend->DibujarMeshBuffer(facesIndex.size(),IndexBufferID);
	rend->TerminarDibujado(0);
	rend->TerminarDibujado(1);
}

void MeshComponent::SetVertices(std::vector<float> vertices)
{
	this->vertices = vertices;
	vertexBufferID = rend->GenerarBuffer(&vertices[0], vertices.size() * sizeof(float));
}

void MeshComponent::SetUVS(std::vector<float> uvs)
{
	this->uvs = uvs;
	uvBufferID = rend->GenerarBuffer(&uvs[0], vertices.size() * sizeof(float));
}

void MeshComponent::SetFaces(std::vector<unsigned short> faces)
{
	facesIndex = faces;
	IndexBufferID = rend->GenerarMeshBuffer(facesIndex,facesIndex.size());
}

void MeshComponent::LoadMaterial()
{
	material = new Material();
	programID = material->CargaShaders("TextureVertexShader.txt", "TextureFragmentShader.txt");
}

void MeshComponent::SetTexture(string texturePath)
{
	texture = loader.CargaBMP(texturePath.c_str());
	textureBufferID = rend->GenerarTextureBuffer(texture.width, texture.height, texture.data);
}
