#include "Transform.h"



Transform::Transform(Renderer* rend):Component(rend),
posicion(0,0,0),
rotacion(0, 0, 0),
escala(0, 0, 0),
matrizMundo(1.0f),
matrizTraslacion(1.0f),
matrizRotacion(1.0f),
matrizEscala(1.0f)
{

}


Transform::~Transform()
{
}

void Transform::Draw(Renderer* rend)
{
	
}

void Transform::Start()
{

}

void Transform::Update()
{
	matrizMundo = matrizTraslacion * matrizRotacion * matrizEscala;
}

void Transform::SetPosicion(float x, float y, float z)
{
	posicion[0] = x;
	posicion[1] = y;
	posicion[2] = z;

	matrizTraslacion = glm::translate(glm::mat4(1.0f), posicion);
	Update();
}

void Transform::SetRotacion(float x, float y, float z)
{
	rotacion[0] = x;
	rotacion[1] = y;
	rotacion[2] = z;

	matrizRotacion = glm::rotate(glm::mat4(1.0f), x, glm::vec3(1.0f, 0.0f, 0.0f));
	matrizRotacion *= glm::rotate(glm::mat4(1.0f), y, glm::vec3(0.0f, 1.0f, 0.0f));
	matrizRotacion *= glm::rotate(glm::mat4(1.0f), z, glm::vec3(0.0f, 0.0f, 1.0f));
	Update();
}

void Transform::SetEscala(float x, float y, float z)
{
	escala[0] = x;
	escala[1] = y;
	escala[2] = z;

	matrizEscala = glm::scale(glm::mat4(1.0f), escala);

	Update();
}

glm::vec3 Transform::GetPosicion()
{
	return posicion;
}
glm::vec3 Transform::GetRotacion()
{
	return rotacion;
}
glm::vec3 Transform::GetEscala()
{
	return escala;
}

glm::mat4 Transform::GetWorldMatrix()
{
	return matrizMundo;
}