#pragma once
#include "Entidad.h"
#include "Material.h"

class DLL_API FormaColoreada :
	public Entidad
{
public:
	FormaColoreada(Renderer * render);
	~FormaColoreada();
	void SetVertices(float * vertices, int count);
	void SetColorVertices(float * vertices, int count);
	void SetTexture(float * vertices, int count);
	void SetMaterial(Material* material);

	virtual void Draw() = 0;
	virtual void Drawing(int _prim);
	void Dispose();

protected:
	Material * material;
	bool shouldDispose; //indica si debo deshacerme del material
	bool shouldDisposeColor;
	bool shouldDisposeUV;
	unsigned int IDBuffer;
	unsigned int IDBufferColor;
	unsigned int IDBufferUV;
	bool buffer;
	bool bufferColor;
	bool bufferTexture;
	float * vertices; //informacion del vertice
	float * verticesColor; //informacion del vertice con color
	float * verticesUV;
	int countVertices;
	int countVerticesColor;
	int countVerticesUV;
	int primitiva;
};

