#include "Rectangulo.h"

Rectangulo::Rectangulo(Renderer *render) :FormaColoreada(render)
{
	primitiva = 0;
	vertices = new float[12]
	{
		-1.0f,-1.0f , 0.0f ,
		1.0f,-1.0f , 0.0f ,
		-1.0f, 1.0f , 0.0f ,
		1.0f, 1.0f , 0.0f
	};

	SetVertices(vertices, 4);

	verticesColor = new float[12]
	{
		0.583f,  0.771f,  0.014f,
		0.609f,  0.115f,  0.436f,
		0.327f,  0.483f,  0.844f,
		0.822f,  0.569f,  0.201f
	};
	SetColorVertices(verticesColor, 4);
}


Rectangulo::~Rectangulo()
{
	Dispose();
}

void Rectangulo::Draw()
{
	Drawing(primitiva);
}
