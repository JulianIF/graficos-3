#pragma once
#include "Export.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "glm\glm.hpp"
#include "GL\glew.h"
#include <vector>
#include "Renderer.h"
class DLL_API ImportedData
{
public:
	ImportedData();
	~ImportedData();
	struct Vertex
	{
		glm::vec3 m_pos;
		glm::vec2 m_tex;
		glm::vec3 m_normal;

		Vertex() {}

		Vertex(const glm::vec3& pos, const glm::vec2& tex, const glm::vec3& normal)
		{
			m_pos = pos;
			m_tex = tex;
			m_normal = normal;
		}
	};

	void Init(const std::vector<Vertex>& Vertices, const std::vector<unsigned short>& Indices, Renderer* renderer);

	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint uvBuffer;

	unsigned int numIndices;
	unsigned int materialIndex;

};

