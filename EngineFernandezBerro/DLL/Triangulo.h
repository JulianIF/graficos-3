#pragma once
#include "FormaColoreada.h"
#include "Material.h"

class DLL_API Triangulo :public FormaColoreada
{
public:
	Triangulo(Renderer * render);
	~Triangulo();
	void Draw() override;
};

