#pragma once
#include "Export.h"
#include "Component.h"
#include "Renderer.h"
#include "Transform.h"
#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
#include <vector>
#include <iostream>


using namespace std;

class DLL_API Node
{
public:
	Node(Renderer * renderer);
	~Node();
	void AddComponent(Component* comp);
	void AddChild(Node* n);
	void Update();
	void Draw();
	void Start();
	Node* GetChildAtIndex(unsigned int i);
	Transform* transform;
	glm::mat4 matTransform;
private:
	vector<Node> children;
	vector<Component*> components;
	Renderer * rend;
	
};

