#pragma once
#include "Export.h"
#include <iostream>
using namespace std;

class DLL_API Ventana
{
public:
	Ventana();
	~Ventana();
	bool Start(int ancho, int alto, const char* n);
	bool Stop();
	bool ShouldClose();
	void PollEvents();
	void* GetVentana();
private:
	void* ventana;
	int ancho;
	int alto;
	const char* titulo;
};

