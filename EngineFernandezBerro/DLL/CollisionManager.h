#pragma once
#include <vector>
#include "Entidad.h"


using namespace std;

class DLL_API CollisionManager
{
private:
	vector<Entidad*> layerA;
	vector<Entidad*> layerB;
	vector<Entidad*> layerC;
public:
	CollisionManager();
	~CollisionManager();
	void AddToLayer(char _layer, Entidad * _e);
	void CheckCollisions();
};

