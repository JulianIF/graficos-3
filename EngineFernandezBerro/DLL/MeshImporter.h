#pragma once
#include "Export.h"
#include "ImportedData.h"
#include <vector>
#include "GL\glew.h"
#include "LoaderTexturas.h"
#include "Node.h"
#include "MeshComponent.h"

class DLL_API MeshImporter
{
public:
	MeshImporter();
	~MeshImporter();
	Node* LoadMesh(const std::string& pFile, const std::string& texturePath, Renderer* renderer);
	
private:

	void InitMesh(aiMesh* mesh, MeshComponent* meshComponent, Renderer* rend);
	int CheckFormat(FILE*& file, unsigned char header[]);
	ImportedData data;
	LoaderTexturas loader;
	void NodeHierarchy(const aiScene* scene, Node* baseNode, aiNode* root, const string& texturePath, Renderer* rend);
};

