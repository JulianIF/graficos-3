#pragma once
#include "Export.h"
#include "Renderer.h"
#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
#include <vector>
#include <iostream>

using namespace std;

class DLL_API Component
{
public:
	Component(Renderer * renderer);
	~Component();
	virtual void Update();
	virtual void Draw(Renderer* rend);
	virtual void Start();
};

