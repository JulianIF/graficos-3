#include "Circulo.h"

#define _USE_MATH_DEFINES
#include <math.h>

Circulo::Circulo(Renderer *render) :FormaColoreada(render),
cantVertices(15),
cantTriangulos(cantVertices - 2),
grados(360.0f / cantTriangulos),
radio(1),
angulo(0)
{
	primitiva = 1;
	vertices = new float[cantVertices];
	DefinirVertices();
	SetVertices(vertices, cantVertices);
}


Circulo::~Circulo()
{
	Dispose();
}

void Circulo::DefinirVertices()
{
	vertices[0] = vertices[1] = vertices[2] = 0.0f;

	for (int i = 3; i < cantVertices * 3; i+=3)
	{
		vertices[i] = cos(angulo) * radio;
		vertices[i + 1] = sin(angulo) * radio;
		vertices[i + 2] = 0.0f;
		angulo += grados * M_PI / 180;
	}
}
void Circulo::Draw()
{
	Drawing(primitiva);
}
