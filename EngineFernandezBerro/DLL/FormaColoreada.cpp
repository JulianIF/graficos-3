#include "FormaColoreada.h"



FormaColoreada::FormaColoreada(Renderer *render) :Entidad(render),
shouldDispose(false),
shouldDisposeColor(false),
shouldDisposeUV(false),
material(NULL),
vertices(NULL),
verticesColor(NULL),
verticesUV(NULL),
IDBuffer(-1),
IDBufferColor(-1),
IDBufferUV(-1),
buffer(false),
bufferColor(false),
bufferTexture(false)
{

}


FormaColoreada::~FormaColoreada()
{
	Dispose();
}

void FormaColoreada::Dispose() //Elimina el buffer, hace delete a los vertices, etc
{
	if (shouldDispose)
	{
		render->DestruirBuffer(IDBuffer);
		buffer = false;
		delete[] vertices;
		shouldDispose = false;
	}
	if(shouldDisposeColor)
	{
		render->DestruirBuffer(IDBufferColor);
		bufferColor = false;
		delete[] verticesColor;
		shouldDisposeColor = false;
	}
	if (shouldDisposeUV)
	{
		render->DestruirBuffer(IDBufferUV);
		bufferTexture = false;
		delete[] verticesUV;
		shouldDisposeUV = false;
	}
}

void FormaColoreada::SetVertices(float* _vertices, int _count)
{
	Dispose();
	countVertices = _count;
	vertices = _vertices;
	shouldDispose = true;
	buffer = true;
	IDBuffer = render->GenerarBuffer(vertices, sizeof(float)* countVertices * 3);
}

void FormaColoreada::SetColorVertices(float* _vertices, int _count)
{
	verticesColor = _vertices;
	countVerticesColor = _count;
	shouldDisposeColor = true;
	bufferColor = true;
	IDBufferColor = render->GenerarBuffer(verticesColor, sizeof(float)* countVerticesColor * 3);
}

void FormaColoreada::SetTexture(float * _vertices, int _count)
{
	verticesUV = _vertices;
	countVerticesUV = _count;
	shouldDispose = true;
	bufferTexture = true;
	IDBufferUV = render->GenerarBuffer(verticesUV, sizeof(float)* countVerticesUV * 2);
}
void FormaColoreada::SetMaterial(Material * _material)
{
	material = _material;
}

void FormaColoreada::Drawing(int _prim) 
{
	render->CargarMatrizIdentidad();
	render->SetMatrizDeMundo(matrizMundo);

	if (material != NULL) 
	{
		material->Bind();
		material->SetMatrizProperty("WVP", render->GetMVP());
	}
	if(buffer)
		render->EmpezarDibujado(0);
	if(bufferColor)
		render->EmpezarDibujado(1);
	if (bufferTexture)
		render->EmpezarDibujado(1);

	if (buffer)
		render->BindBuffer(IDBuffer, 0);
	if (bufferColor)
		render->BindBuffer(IDBufferColor, 1);
	if (bufferTexture)
		render->BindUVBuffer(IDBufferUV, 1);

	render->DibujarBuffer(countVertices, primitiva);

	if (buffer)
		render->TerminarDibujado(0);
	if (bufferColor)
		render->TerminarDibujado(1);
	if (bufferTexture)
		render->TerminarDibujado(1);
}
