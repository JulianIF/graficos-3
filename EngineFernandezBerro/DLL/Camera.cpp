#include "Camera.h"



Camera::Camera(Renderer * renderer) :Entidad(renderer)
{
	up = glm::vec3(0, 1, 0);
	forward = glm::vec3(0, 0, 1);
	right = glm::vec3(1, 0, 0);
}


Camera::~Camera()
{
}


void Camera::Walk(float dir)
{
	this->SetPosicion(this->GetPosicion().x + dir * forward.x, this->GetPosicion().y + dir * forward.y, this->GetPosicion().z + dir * forward.z);
	UpdateView();
}

void Camera::Strafe(float dir)
{
	this->SetPosicion(this->GetPosicion().x + dir * right.x, this->GetPosicion().y + dir * right.y, this->GetPosicion().z + dir * right.z);
	UpdateView();
	cout << this->GetPosicion().x << endl;
}

void Camera::Pitch(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::vec4 aux (forward.x, forward.y, forward.z, 0);
	aux = rot * aux;
	forward = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(up.x, up.y, up.z, 0);
	aux = rot * aux;
	up = glm::vec3(aux.x, aux.y, aux.z);

	UpdateView();
}

void Camera::Roll(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(0.0f, 0.0f, 1.0f));
	glm::vec4 aux(right.x, right.y, right.z, 0);
	aux = rot * aux;
	right = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(up.x, up.y, up.z, 0);
	aux = rot * aux;
	up = glm::vec3(aux.x, aux.y, aux.z);

	UpdateView();
}

void Camera::Yaw(float deg)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), deg, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::vec4 aux(forward.x, forward.y, forward.z, 0);

	aux = rot * aux;
	forward = glm::vec3(aux.x, aux.y, aux.z);

	aux = glm::vec4(right.x, right.y, right.z, 0);
	aux = rot * aux;
	right = glm::vec3(aux.x, aux.y, aux.z);

	UpdateView();
}
void Camera::UpdateView()
{
	up = glm::vec3(0, 1, 0);
	render->SetViewMatrix(this->GetPosicion(), this->GetPosicion() + forward, up);
}

void Camera::Draw()
{
	UpdateView();
}