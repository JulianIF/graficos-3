#pragma once
#include "FormaColoreada.h"
#pragma once
#include "FormaColoreada.h"

class DLL_API Circulo :
	public FormaColoreada
{
public:
	Circulo(Renderer * render);
	~Circulo();
	void Draw() override;
private:
	int cantVertices;
	int cantTriangulos;
	float angulo;
	float grados;
	int radio;
	void DefinirVertices();
};

