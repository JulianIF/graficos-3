#pragma once
#include "Export.h"
#include "Renderer.h"
#include "BoxCollider.h"
#include <iostream>
using namespace std;

class DLL_API Entidad
{
public:
	Entidad(Renderer * rendererPTR);
	virtual void Draw() = 0;
	BoxCollider collider;
	//SETTERS
	void SetPosicion(float x, float y, float z);
	void SetRotacion(float x, float y, float z);
	void SetEscala(float x, float y, float z);
	//GETTERS
	glm::vec3 GetPosicion();
	glm::vec3 GetRotacion();
	glm::vec3 GetEscala();
	~Entidad();
private:
	glm::vec3 posicion;
	glm::vec3 rotacion;
	glm::vec3 escala;
protected:
	Renderer * render;
	glm::mat4 matrizMundo;
	glm::mat4 matrizTraslacion;
	glm::mat4 matrizRotacion;
	glm::mat4 matrizEscala;
	void UpdateMatrizDeMundo();
};

