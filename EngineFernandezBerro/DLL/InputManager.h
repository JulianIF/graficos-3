#pragma once
#include "Export.h"
#include "Ventana.h"
#include "GLFW/glfw3.h"
class DLL_API InputManager
{
public:
	InputManager(Ventana* _window);
	~InputManager();
	Ventana* window;

public:
	bool isInput(int key);

	void SetWindowContext(Ventana* window);
};

