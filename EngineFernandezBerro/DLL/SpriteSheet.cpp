#include "SpriteSheet.h"



SpriteSheet::SpriteSheet(int columns, int rows) :
	totalHeight(1.0f),
	totalWidth(1.0f)
{
	uvVector = new vector<float*>();
	float frameW = 1.0f / columns;
	float frameH = 1.0f / rows;
	int totalSprites = columns* rows;

	for (int i = 0; i < totalSprites; i++) 
	{
		float x = (i % columns) * frameW;
		float y = (i % rows) * frameH;

		uv = new float[8]
		{
			x / totalWidth, 1 - ((y + frameH) / totalHeight),
			x / totalWidth, 1 - y / totalHeight,
			(x + frameW) / totalWidth , 1 - ((y + frameH) / totalHeight),
			(x + frameW) / totalWidth, 1 - (y / totalHeight)
		};

		uvVector->push_back(uv);
	}
}

SpriteSheet::~SpriteSheet()
{
	delete uvVector;
}

float* SpriteSheet::GetFrame(int index)
{
	return uvVector->at(index);
}
int SpriteSheet::GetSize()
{
	return uvVector->size();
}
