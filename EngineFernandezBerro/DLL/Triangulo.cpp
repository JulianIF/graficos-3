#include "Triangulo.h"



Triangulo::Triangulo(Renderer *render) :FormaColoreada(render)
{
	primitiva = 0;
	vertices = new float[9]
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f,
	};

	SetVertices(vertices, 3);

	verticesColor = new float[9]
	{
		0.709f,  0.115f,  0.436f,
		0.327f,  0.583f,  0.844f,
		0.822f,  0.569f,  0.201f
	};

	SetColorVertices(verticesColor, 3);
}


Triangulo::~Triangulo()
{
	Dispose();
}

void Triangulo::Draw()
{
	Drawing(primitiva);
}
