#include "Mesh.h"



Mesh::Mesh(Renderer * renderer, const string modelPath, string texturePath, MeshImporter* imp) :FormaColoreada(renderer)
{
	primitiva = 0;
	importer = imp;
	float* vertices = new float;

	LoadMesh(modelPath, texturePath);

	verticesColor = new float[24]
	{
		0.583f,  0.771f,  0.014f,
		0.609f,  0.115f,  0.436f,
		0.327f,  0.483f,  0.844f,
		0.822f,  0.569f,  0.201f,
		0.583f,  0.771f,  0.014f,
		0.609f,  0.115f,  0.436f,
		0.327f,  0.483f,  0.844f,
		0.822f,  0.569f,  0.201f
	};
	SetColorVertices(verticesColor, 8);
}


Mesh::~Mesh()
{
}

void Mesh::Draw()
{
	render->CargarMatrizIdentidad();
	render->SetMatrizDeMundo(matrizMundo);
	for (int i = 0; i < bufferTextures.size(); i++)
	{
		if (material != NULL)
		{
			material->Bind();
			material->BindTexture("myTextureSampler", bufferTextures[i]);
			material->SetMatrizProperty("WVP", render->GetMVP());
		}
	}
	//if (buffer)
		render->EmpezarDibujado(0);
	if (bufferColor)
		render->EmpezarDibujado(1);
	for (unsigned int i = 0; i < m_Entries.size(); i++) {

		render->BindBuffer(m_Entries[i].vertexBuffer, 0);
		render->BindUVBuffer(m_Entries[i].uvBuffer, 1);
		render->DibujarMeshBuffer(m_Entries[i].numIndices, m_Entries[i].indexBuffer);
	}
	//if (buffer)
		render->TerminarDibujado(0);
	if (bufferColor)
		render->TerminarDibujado(1);
}

void Mesh::SetIndices(std::vector<unsigned short> _indices, int _count)
{
	//Dispose();
	countIndices = _count;
	indices = _indices;
	shouldDispose = true;
	buffer = true;
	IDMeshBuffer = render->GenerarMeshBuffer(indices, countIndices);
}

bool Mesh::LoadMesh(const string modelPath, string texturePath)
{/*
	bool state = importer->LoadMesh(modelPath, texturePath, m_Entries, m_Textures, render);
	for (int i = 0; i < m_Textures.size(); i++)
		bufferTextures.push_back(render->GenerarTextureBuffer(m_Textures[i].width, m_Textures[i].height, m_Textures[i].data));
	return state;*/
	return false;
}