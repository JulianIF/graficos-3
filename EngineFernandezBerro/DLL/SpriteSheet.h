#pragma once
#include "Export.h"
#include <vector>
using namespace std;

class DLL_API SpriteSheet
{
private:
	float * uv;
	vector<float*> * uvVector;
	int totalHeight;
	int totalWidth;

public:
	SpriteSheet(int columns, int rows);
	~SpriteSheet();
	float* GetFrame(int index);
	int GetSize();
};

