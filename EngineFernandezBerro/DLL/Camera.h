#pragma once
#include "Entidad.h"
#include "Export.h"
class DLL_API Camera :
	public Entidad
{
public:
	Camera(Renderer * rendererPTR);
	~Camera();

	void Walk(float dir);
	void Strafe(float dir);
	void Pitch(float deg);
	void Roll(float deg);
	void Yaw(float deg);
	void UpdateView();
	virtual void Draw();
private:
	glm::vec3 up;
	glm::vec3 forward;
	glm::vec3 right;
	glm::mat4 rot;
};

